const quotes = document.getElementById("quotes")
const author = document.getElementById("author")
const quoteGenerator = document.getElementById("generator")

// when button clicks run add evntlistener
quoteGenerator.addEventListener('click', gQuotes)

// inside the function
function gQuotes(){

    // api called
    fetch('https://api.quotable.io/random')
    .then((response)=> response.json())
    .then((randomQuotes)=> {
    
    // change dislay none to block
    let div = document.getElementById('divQuotes');
    div.style.display = 'block';
    // append the api details to our website
    quotes.innerHTML = randomQuotes.content
    author.innerHTML = randomQuotes.author
})
.catch((error)=> console.log(error))
}












